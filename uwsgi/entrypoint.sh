#!/bin/sh
usage() {
  echo -e "Usage\n\t${0##*/} "
  exit 1
}
echo_err() { logger "$@"; echo "$@" >&2; }
main() {
  if [ -z "$1" ]; then
    usage
  fi

  run_mode "$1"
}
_setup() {
  :
}
_start() {
  :
}
_stop() {
  :
}
_kill() {
  :
}
_reload() {
  :
}
_restart() {
  _stop
  _start
}
_remove() {
  :
}

run_mode() {
  case $1 in 
    start)
      _setup
      _start
      ;;
    stop)
      _stop
      ;;
    reload)
      _reload
      ;;
    restart)
      _restart
      ;;
    remove)
      _remove
      ;;
    *)
      usage
      ;;
  esac
}

main "$@"
