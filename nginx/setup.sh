#!/bin/bash
# build docker image and install configuration
usage() {
  cat << EOF
Usage  ${0##*/} (start|stop|reload|restart|kill|setup)


EOF
  exit 1
}
echo_err() { logger "$@"; echo "$@" >&2; }
main() {
  if [ -z "$1" ]; then
    usage
  fi
  run_mode "$@"
}
_setup() {
  source ./service.env && rm -rf docker-compose.yml && envsubst < "template.yml" > "docker-compose.yml";

  docker-compose build
}
_start() {
  docker-compose up
}
_stop() {
  docker-compose stop
}
_kill() {
  docker-compose kill
}
_reload() {
  :
}
_restart() {
  _stop
  _start
}
_remove() {
  docker-compose rm
}

run_mode() {
  case $1 in 
    start)
      _setup
      _start
      ;;
    stop)
      _stop
      ;;
    reload)
      _reload
      ;;
    kill)
      _kill
      ;;
    restart)
      _restart
      ;;
    remove)
      _remove
      ;;
    setup)
      _setup 1
      ;;
    *)
      exec "$@"
      exit 0
      ;;
  esac
}

main "$@"
