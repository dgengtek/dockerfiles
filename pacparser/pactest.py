#!/usr/bin/python2.5

import pacparser

pacparser.init()
pacparser.parse_pac("wpad.dat")
proxy = pacparser.find_proxy("http://google.com")
print proxy
pacparser.cleanup()

# Or simply,
print pacparser.just_find_proxy("wpad.dat", "http://google.com")
