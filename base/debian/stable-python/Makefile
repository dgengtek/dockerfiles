DOCKER_IMAGE_VERSION = 1
DOCKER_CONTAINER_NAME = debianstablepython3
DOCKER_IMAGE_AUTHOR= dgengtek
DOCKER_IMAGE_TAG = ${DOCKER_IMAGE_AUTHOR}/${DOCKER_CONTAINER_NAME}
DOMAIN = ${DOMAIN_INTRANET}
DOCKER_RUN_OPTIONS = -d
DOCKER_BUILD_ARGS = --build-arg="author=${DOCKER_IMAGE_AUTHOR}" --build-arg="http_proxy=${http_proxy}"
DOCKER_REGISTRY = docker
GIT_ROOTDIR := $(shell git rev-parse --show-toplevel)
DOCKER_BUILD_PATH := $(shell pwd | sed "s,${GIT_ROOTDIR}/,,")
DOCKER_IMAGE_NAME = ${DOCKER_BUILD_PATH}

HOSTNAME = minion.debian
DOMAIN = $(shell hostname -d)

.PHONY = all bash run rm rmi stop kill cleanup clean logs push

all: build

build:
	docker build ${DOCKER_BUILD_ARGS} -t ${DOCKER_IMAGE_NAME}:v${DOCKER_IMAGE_VERSION} .

push:
	docker tag ${DOCKER_IMAGE_NAME}:v${DOCKER_IMAGE_VERSION} ${DOCKER_REGISTRY}.${DOMAIN}/${DOCKER_IMAGE_NAME}:v${DOCKER_IMAGE_VERSION}
	docker push ${DOCKER_REGISTRY}.${DOMAIN}/${DOCKER_IMAGE_NAME}:v${DOCKER_IMAGE_VERSION}

run: build
	rand=$$(date +'%Y%m%d%H%M%S') && \
	docker run ${DOCKER_RUN_OPTIONS} -h "${HOSTNAME}.$${rand}.${DOMAIN}" --name ${DOCKER_CONTAINER_NAME} --rm -it ${DOCKER_IMAGE_NAME}:v${DOCKER_IMAGE_VERSION}

bash: run
	docker exec -it ${DOCKER_CONTAINER_NAME} bash

stop:
	docker stop ${DOCKER_CONTAINER_NAME}

kill:
	docker kill ${DOCKER_CONTAINER_NAME}

rm: stop
	docker rm ${DOCKER_CONTAINER_NAME}

rmi: rm
	docker rmi ${DOCKER_IMAGE_NAME}:v${DOCKER_IMAGE_VERSION}

logs:
	docker logs ${DOCKER_CONTAINER_NAME}

get_ip:
	docker inspect ${DOCKER_CONTAINER_NAME} | grep -e IPAddress -e MacAddress

get_port:
	docker inspect ${DOCKER_CONTAINER_NAME} | grep HostPort

cleanup: rmi

clean: cleanup
