#!/bin/env bash
main() {
    ssh-keygen -A
    local -r PASS=$RANDOM
    echo "$PASS"
    echo "root:$PASS" | chpasswd && \


    /usr/sbin/sshd -D
}

main "$@"
