#!/bin/bash
# TODO more verbose output of instructions
usage() {
  echo -e "Usage\n\t${0##*/} "
  exit 1
}
echo_err() { logger "$@"; echo "$@" >&2; }

main() {
  if [ -z "$1" ]; then
    usage
  fi
  ################################################################################
  # App common settings
  local -r APP_NAME=${APP_NAME:-"homepage"}
  # Environment setup settings
  local -r PROJECT_ROOT=${DJANGO_PROJECT_ROOT:?"error, no root path for app deployment set"}

  # in alpine different syntax ...
  local -r project_basename=$(basename "$APP_REPO_URI" ".git")
  local -r DJANGO_PROJECT_URL="$PROJECT_ROOT/homepage"

  export APP_NAME
  export PROJECT_ROOT
  export DJANGO_PROJECT_URL


  # Django project environment settings
  local -r DJANGO_CONFIG_ROOT="/usr/local/etc/$APP_NAME"
  local -r DJANGO_MEDIA_ROOT=${DJANGO_MEDIA_ROOT:-"/srv/media"}
  local -r DJANGO_STATIC_ROOT="${DJANGO_MEDIA_ROOT}/../static"
  local -r DJANGO_SETTINGS_DATABASE="sqlite3"

  export DJANGO_CONFIG_ROOT
  export DJANGO_MEDIA_ROOT
  export DJANGO_STATIC_ROOT
  export DJANGO_SETTINGS_DATABASE

  # etc
  local -r uwsgi_bin=uwsgi
  local -r uwsgi_ini_path="${DJANGO_CONFIG_ROOT}/${APP_NAME}.ini"
  ################################################################################

  run_mode "$@"
}

_fix_permissions() {
  # permissions
  if [ -z "$2" ]; then
    pdir=550
  else
    pdir=$2
  fi
  if [ -z "$3" ]; then
    pfile=440
  else
    pfile=$3
  fi
  find "$1" -type d -print0 | xargs -0 -I f chmod $pdir f
  find "$1" -type f -print0 | xargs -0 -I f chmod $pfile f

  chown -R $user:$user "$1"
}
_setup() {
  # do nothing if exists and not forced
  if [[ $1 != "-f" ]] && [[ -d $PROJECT_ROOT ]]; then
    return 0
  else
    mkdir -p "$DJANGO_MEDIA_ROOT"
    mkdir -p "$DJANGO_STATIC_ROOT"
    mkdir -p "$DJANGO_CONFIG_ROOT"
    # project url contains root
    # mkdir -p "$PROJECT_ROOT"
    mkdir -p "$DJANGO_PROJECT_URL"
  fi 
  echo "if ! (mv -f /project "$DJANGO_PROJECT_URL" && pushd "$DJANGO_PROJECT_URL" \
    && mv project/* . && rmdir project && popd); then"
  # move django project
  cp -rv /project "$DJANGO_PROJECT_URL" 
  pushd "$DJANGO_PROJECT_URL" || (echo "push dir fail";exit 1)
  mv -f project/* . && rm -rf project && rm -rf /project
  popd

  if ! (cp "$DJANGO_PROJECT_URL/${APP_NAME}.ini" "$uwsgi_ini_path"); then
    echo_err "Failed setup of configuration files" 
    exit 1
  fi
  cp /deploy/* "$DJANGO_CONFIG_ROOT/" && rm -rfv /deploy && echo "Copied deployment files"
  local -r user=$(grep uid "$uwsgi_ini_path" | sed -E 's:uid = (.*):\1:')

  pushd "$DJANGO_PROJECT_URL/$APP_NAME"
  if ! bash setup.sh "$@" setup; then
    echo_err "Project setup failed." 
    exit 1
  fi
  popd

  # move to new destination and remove empty dir structure
  rsync -am "$DJANGO_PROJECT_URL/static/" "$DJANGO_STATIC_ROOT/" \
    && rm -rf "$DJANGO_PROJECT_URL/static" && echo "Relocated static files"

  # alpine add system user 
  adduser -S "$user"
  addgroup -S "$user"

  # default d:550, f:440
  _fix_permissions "$PROJECT_ROOT" 
  _fix_permissions "$DJANGO_MEDIA_ROOT" "555" "444"
  _fix_permissions "$DJANGO_STATIC_ROOT" "555" "444"
  chmod 550 "$DJANGO_PROJECT_URL"

  if [[ "$DJANGO_SETTINGS_DATABASE" == "sqlite3" ]]; then
    local dbname=$(grep "DATABASE_NAME" $DJANGO_CONFIG_ROOT/database.ini)
    dbname=$(echo ${dbname##*=} | xargs)
    chmod 660 "${DJANGO_PROJECT_URL}/$APP_NAME/${dbname}"
  fi
}
_start() {
  $uwsgi_bin "$uwsgi_ini_path"
}
_stop() {
  $uwsgi_bin --stop "$uwsgi_ini_path"
}
_kill() {
  :
}
_reload() {
  $uwsgi_bin --reload "$uwsgi_ini_path"
}
_restart() {
  _stop && _start
}
_remove() {
  :
}

run_mode() {
  case $1 in 
    start)
      _setup "$@"
      _start "$@"
      ;;
    stop)
      _stop
      ;;
    reload)
      _reload
      ;;
    restart)
      _restart
      ;;
    remove)
      _remove
      ;;
    setup)
      shift
      _setup "-f" "$@"
      ;;
    *)
      exec "$@"
      exit 0
      ;;
  esac
}

main "$@"
