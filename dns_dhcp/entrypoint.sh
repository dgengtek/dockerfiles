#!/bin/sh
main() {
  run_mode "$@"
}
setup() {
  if [ -d /run/named ]; then
    return 1
  fi
  mkdir -p /run/named
  cp -Rv /tmp/named/* /var/named/
  rm -rvf /tmp/named

  touch /run/named/named.pid
  touch /var/lib/dhcp/dhcpd.leases
  touch /var/run/dhcp/dhcpd.pid

}
_start() {
  exec /usr/bin/supervisord -n
}
_stop() {
  /usr/bin/supervisorctl shutdown
}
_reload() {
  :
}
_restart() {
  _stop
  _start
}

run_mode() {
  setup
  case $1 in 
    start)
      _start
      ;;
    stop)
      _stop
      ;;
    reload)
      _reload
      ;;
    restart)
      _restart
      ;;
    *)
      exec "$@"
      exit 0
      ;;
  esac
}
trap _stop SIGINT SIGHUP SIGTERM
main "$@"
