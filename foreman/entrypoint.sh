#!/bin/bash
usage() {
  echo -e "Usage\n\t${0##*/} "
  exit 1
}
echo_err() { logger "$@"; echo "$@" >&2; }
main() {
  if [ -z "$1" ]; then
    usage
  fi

  run_mode "$@"
}
_setup() {
  chown -R foreman:foreman /var/cache/foreman
}
_start() {
  local -r service_path=/etc/init.d
  local -a services
  services=(
  "foreman"
  "salt-master"
  "salt-minion"
  )
  for s in ${services[@]}; do
    $service_path/$s start
  done
}
_stop() {
  :
}
_kill() {
  :
}
_reload() {
  :
}
_restart() {
  _stop
  _start
}
_remove() {
  :
}

run_mode() {
  case $1 in 
    start)
      _setup
      _start
      ;;
    stop)
      _stop
      ;;
    reload)
      _reload
      ;;
    restart)
      _restart
      ;;
    remove)
      _remove
      ;;
    *)
      exec "$@"
      exit 0
      ;;
  esac
}

main "$@"
