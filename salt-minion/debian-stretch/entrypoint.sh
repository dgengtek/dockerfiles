#!/bin/bash
usage() {
  cat << EOF
Usage  ${0##*/} 


EOF
  exit 1
}
echo_err() { logger "$@"; echo "$@" >&2; }
main() {
  if [ -z "$1" ]; then
    usage
  fi
  run_mode "$@"
}
_setup() {
  :
}
_start() {
  salt-minion 
}
_stop() {
  :
}
_kill() {
  :
}
_reload() {
  :
}
_restart() {
  _stop
  _start
}
_remove() {
  :
}

run_mode() {
  mode=$1
  shift
  case $mode in 
    setup)
      _setup $@
      ;;
    start)
      _setup $@
      shift 
      _start $@
      ;;
    stop)
      _stop
      ;;
    reload)
      _reload
      ;;
    restart)
      _restart
      ;;
    remove)
      _remove
      ;;
    *)
      exec $mode $@
      ;;
  esac
}

main "$@"
