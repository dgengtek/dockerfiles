#!/bin/bash
# build docker image and install configuration
usage() {
  cat << EOF
Usage  ${0##*/} mode
  
  mode:
    start
    stop
    reload
    restart
    kill
    setup

    (client modes)
    ccrt-create clientname
    ccrt-get clientname
    ccrt-revoke clientname
    ccrt-list-all
EOF
  exit 1
}
echo_err() { logger "$@"; echo "$@" >&2; }
main() {
  if [ -z "$1" ]; then
    usage
  fi
  trap _trap_sig SIGINT
  source ./service.env && rm -rf docker-compose.yml && envsubst < "template.yml" > "docker-compose.yml";
  _check_env

  local -r openvpn_image="dgeng/openvpn:latest"
  run_mode "$@"
}
_check_env_exists() {
  [ -n "$1" ]
}
_check_env() {
  for e in $OVPN_DATA $OVPN_SERVER $CLIENTNAME $VPN_SERVER_NAME; do
    if ! _check_env_exists "$e";then 
      echo "An Env var is not set"
      exit 1
    fi
  done
}
_trap_sig() {
  exit 1
}
_setup() {
  _wrap_echo "Building $OVPN_DATA"
  docker-compose build $OVPN_SERVER
  _wrap_echo "Start $OVPN_DATA"
  docker-compose up -d "$OVPN_DATA"
  _wrap_echo "Generate config"
  docker run --volumes-from "$OVPN_DATA" --rm $openvpn_image ovpn_genconfig \
    -u udp://$VPN_SERVER_NAME
  _wrap_echo "Init pki"
  docker run --volumes-from "$OVPN_DATA" --rm -it $openvpn_image ovpn_initpki

  _create_client_cert "$CLIENTNAME"
  _get_client_cert "$CLIENTNAME"
}
_run() {
  _start
  echo "$@"
  docker exec -it "$OVPN_SERVER" $@

}
_create_client_cert() {
  _wrap_echo "Generate client cert: $1"
  docker run --volumes-from "$OVPN_DATA" --rm -it $openvpn_image easyrsa \
  build-client-full "$1"
}
_get_client_cert() {
  _wrap_echo "Get client cert: $1"
  docker run --volumes-from $OVPN_DATA --rm $openvpn_image ovpn_getclient \
  "$1" > "$1".ovpn
}
_revoke_client_cert() {
  _wrap_echo "Revoking client cert: $1"
  docker run --volumes-from $OVPN_DATA --rm -it $openvpn_image easyrsa \
  revoke "$1" && easyrsa gen-crl
}
_list_client_certs() {
  _wrap_echo "Listing client certs"
  docker run --volumes-from $OVPN_DATA --rm $openvpn_image ovpn_listclients
}
_wrap_echo() {
  echo -n "==========> "
  echo "$@"
}
_start() {
  docker-compose up -d $OVPN_SERVER
}
_stop() {
  docker-compose stop
}
_kill() {
  docker-compose kill
}
_reload() {
  :
}
_restart() {
  _stop
  _start
}
_remove() {
  _stop
  docker-compose rm --all
}

run_mode() {
  mode=$1
  case "$mode" in 
    start)
      _start
      ;;
    stop)
      _stop
      ;;
    reload)
      _reload
      ;;
    kill)
      _kill
      ;;
    restart)
      _restart
      ;;
    remove)
      _remove
      ;;
    ccrt-create)
      shift 1
      _create_client_cert "$1"
      ;;
    ccrt-get)
      shift 1
      _get_client_cert "$1"
      ;;
    ccrt-revoke)
      shift 1
      _revoke_client_cert "$1"
      ;;
    ccrt-list-all)
      _list_client_certs 
      ;;
    setup)
      _setup 1
      ;;
    *)
      _run $@
      exit 0
      ;;
  esac
}

main "$@"
