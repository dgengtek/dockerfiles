#!/bin/env bash
usage() {
  cat << EOF
Usage: ${0##*/} mode

mode:
    start | run
    rm | remove
EOF
}

main() {
  local image="dgeng/salt-minion:python3.5"
  local docker_bin="/usr/bin/docker"
  local docker_cmd="$docker_bin run -d "

  local -r name="test.homepage.docker"
  local service_ids=(
  "$name"
  )

  trap cleanup SIGINT SIGTERM EXIT

  case $1 in
    start|run)
      run
      ;;
    stop)
      ;;
    kill)
      ;;
    rm|remove)
      cleanup
      ;;
    *)
      usage
      exit 1
      ;;

  esac

}

run() {
  # homepage
  docker_run "$name" "-p 0.0.0.0:80:80" "--expose=60000"
  sleep 10
  salt-key --yes -a "$name"
  sleep 15
  salt "$name" saltutil.sync_all
  salt "$name" state.highstate --state-output=mixed 
  echo "Enter into sleep"
  while :; do
    sleep 10
  done

}
docker_run() {
  hostname=$1
  shift 
  $($docker_cmd -h $hostname --name $hostname $@ $image start $hostname &> /dev/null)
  echo -n "$hostname "
  if (($? == 0)); then
    echo "created"
  else
    echo "failed to create"
    cleanup_container "$hostname"
    exit 1
  fi
  echo "Attaching to $hostname..."
  $($docker_bin attach "$hostname") &
}
cleanup() {
  trap - SIGINT SIGTERM EXIT
  salt-key --yes -d "$name"
  for id in ${service_ids[@]}; do
    cleanup_container "$id"
  done
  exit 1
}
cleanup_container() {
    check_container "$id"
    result=$?
    if [[ $result == 0 ]]; then
      echo "Kill container with id: $id."
      docker kill "$id" &> /dev/null
    fi
    docker rm "$id" &> /dev/null
}
check_container() {
  local result=$(docker inspect -f {{.State.Running}} $1)
  if [[ $result == "true" ]]; then
    return 0
  elif [[ $result  == "false" ]]; then
    return 1
  else
    # does not exist
    return 2
  fi
}

main $@
