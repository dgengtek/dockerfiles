#!/bin/env bash
usage() {
  cat << EOF
Usage: ${0##*/} mode

mode:
    start | run
    rm | remove
EOF
exit 1

}

main() {
  local service_ids=(
  "web.docker.base"
  "homepage.docker.base"
  "database.docker.base"
  )
  local image="dgeng/salt-minion:python3.5"
  local docker_bin="/usr/bin/docker"
  local docker_cmd="$docker_bin run -d "

  trap cleanup SIGINT SIGTERM EXIT

  case $1 in
    start|run)
      run
      ;;
    stop)
      ;;
    kill)
      ;;
    rm|remove)
      cleanup
      ;;
    *)
      usage
      ;;

  esac

}

run() {

  # db
  docker_run "database.docker.base" "--expose=5432" "-p 0.0.0.0:5432:5432"
  # homepage
  docker_run "homepage.docker.base" "-p 0.0.0.0:8000:8000" "--expose=60000" \
  "--link=database.docker.base" "-v /data/docker/homepage/log:/var/log/uwsgi" \
  "-v /srv/app/srv/static" "-v /srv/app/srv/media"
  # web
  docker_run "web.docker.base" "-p 0.0.0.0:80:80" "-p 0.0.0.0:443:443" \
  "--link=homepage.docker.base" "-v /data/docker/homepage/log:/var/log/nginx" \
  "-v /data/docker/homepage/log:/var/log/nginx" \
  "--volumes-from=homepage.docker.base:ro"


  echo "Enter into sleep"
  while :; do
    sleep 10
  done

}
docker_run() {
  hostname=$1
  shift 
  $($docker_cmd -h $hostname --name $hostname $@ $image start $hostname &> /dev/null)
  echo -n "$hostname "
  if (($? == 0)); then
    echo "created"
  else
    echo "failed to create"
    cleanup_container "$hostname"
    exit 1
  fi
  echo "Attaching to $hostname..."
  $($docker_bin attach "$hostname") &
}
cleanup() {
  trap - SIGINT SIGTERM EXIT
  for id in ${service_ids[@]}; do
    cleanup_container "$id"
  done
  exit 1
}
cleanup_container() {
    check_container "$id"
    result=$?
    if [[ $result == 0 ]]; then
      echo "Kill container with id: $id."
      docker kill "$id" &> /dev/null
    fi
    if [[ $result != 2 ]]; then
      echo "Remove container with id: $id."
      docker rm "$id" &> /dev/null
    fi
}
check_container() {
  local result=$(docker inspect -f {{.State.Running}} $1)
  if [[ $result == "true" ]]; then
    return 0
  elif [[ $result  == "false" ]]; then
    return 1
  else
    # does not exist
    return 2
  fi
}

main $@
