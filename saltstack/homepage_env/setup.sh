#!/bin/bash
# build docker image and install configuration
# template the compose file to create several hosts
usage() {
  cat << EOF
Usage  ${0##*/} (start|stop|reload|restart|kill|setup)


EOF
  exit 1
}
echo_err() { logger "$@"; echo "$@" >&2; }
main() {
  if [ -z "$1" ]; then
    usage
  fi
  trap cleanup SIGKILL SIGTERM EXIT
  run_mode "$@"
}
_setup() {
  docker-compose up -d --force-recreate
}
_start() {
  docker-compose start
  echo "Sleep for startup to receive salt keys"
  sleep 10
  salt-key --yes -a "*docker*" || exit
  echo "Sleep so salt minions can connect to salt master after key got accepted"
  sleep 10
  update
}
update() {
  salt "*docker*" saltutil.sync_all
  echo "Starting highstate of docker images"
  salt "*docker*" state.highstate --state-output=mixed 
}
_stop() {
  docker-compose stop
  _remove
}
_kill() {
  docker-compose kill
}

cleanup() {
  trap - SIGKILL SIGTERM EXIT
  _remove
  kill $$
}
_reload() {
  :
}
_restart() {
  _remove
  _setup
  _start
}
_remove() {
  salt-key --yes -d "*docker*"
  docker-compose down
}

run_mode() {
  case $1 in 
    start|run)
      _setup
      _start
      ;;
    stop|down)
      _stop
      ;;
    reload)
      _reload
      ;;
    kill)
      _kill
      ;;
    restart)
      _restart
      ;;
    remove|rm)
      _remove
      ;;
    update)
      update
      ;;
    setup)
      _setup 1
      ;;
    *)
      echo "Not a valid mode."
      exit 0
      ;;
  esac
}

main "$@"
