#!/bin/bash
# build buildbot environment and images
usage() {
  cat << EOF
Usage  ${0##*/} (start|stop|reload|restart|kill|setup)


EOF
  exit 1
}

main() {
  if [ -z "$1" ]; then
    usage
  fi
  run_mode "$@"
}
_setup() {
  local master_directory="master"
  local worker_directory="worker"
  local -a distributions=(
  "centos7"
  "ubuntu1604"
  )

  [[ -f Makefile.template ]] || die "Missing Makefile template"

  # build per distribution the master and worker first
  for distribution in "${distributions[@]}"; do
    for service_type in $master_directory $worker_directory; do
      if ! _build_environment "$service_type" "$distribution"; then
        error "Setup environment for $service_type and $distribution failed."
      fi
    done
  done
}

_build_environment() {
  local path=$1
  local distribution=$2
  local -r distribution_path="${path}/${distribution}"
  local -r cwd=$PWD
  ! [[ -f ${path}/entrypoint_${path}.sh ]] && error "Entrypoint for $path missing." && return 1
  ! [[ -d $distribution_path ]] && error "$distribution_path does not exist." && return 1 
  export TEMPLATE_DOCKER_CONTAINER_NAME=
  cp -f "${path}/entrypoint_${path}.sh" "${distribution_path}/entrypoint_${path}.sh"
  _build_makefile || return 1
  # build image
  make -C "$distribution_path" build
}

_build_makefile() {
  {
    [[ -f ${distribution_path}/Dockerfile ]] || error "Dockerfile missing in $distribution_path. Skipping build."
  } || return 1
  export TEMPLATE_DOCKER_CONTAINER_NAME="buildbot-$path-base"
  export TEMPLATE_DOCKER_DISTRIBUTION=$distribution
  export TEMPLATE_DOCKER_HOSTNAME="buildbot-${path}.docker"
  envsubst '$TEMPLATE_DOCKER_CONTAINER_NAME $TEMPLATE_DOCKER_DISTRIBUTION $TEMPLATE_DOCKER_HOSTNAME' < "Makefile.template" > "${distribution_path}/Makefile";
}

_start() {
  :
}
_stop() {
  :
}
_kill() {
  :
}
_reload() {
  :
}
_restart() {
  :
}
_remove() {
  :
}

run_mode() {
  case $1 in 
    start)
      _setup
      _start
      ;;
    stop)
      _stop
      ;;
    reload)
      _reload
      ;;
    kill)
      _kill
      ;;
    restart)
      _restart
      ;;
    remove)
      _remove
      ;;
    setup)
      _setup 1
      ;;
    *)
      exec "$@"
      exit 0
      ;;
  esac
}

log() { echo "$@" | logger -s -t ${0##*/}; }
out() { echo "$1 $2" "${@:3}"; }

info() { log "==> INFO:" "$@"; } >&2
error() { log "==> ERROR:" "$@"; } >&2
warning() { log "==> WARNING:" "$@"; } >&2
critical() { log "==> CRITICAL:" "$@"; } >&2
errorq() { log "$@" 2>/dev/null; }

msg() { log "==>" "$@"; }
msg2() { log "  ->" "$@"; }

error_exit() {
  error_code=$1
  shift
  error "$@"
  exit $error_code
}
die() { error_exit 1 "$@"; }

main "$@"
