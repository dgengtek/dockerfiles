CONCOURSE_TARGET = intranet
CONCOURSE_CONFIG_TEMPLATE = ./ci/concourse.yml.j2
CONCOURSE_CONFIG_DATA = ./ci/concourse-data.yml
CONCOURSE_CONFIG = ./ci/concourse.yml
CONCOURSE_PIPELINE = dockerfiles
CONCOURSE_PARAMETERS = ./ci/concourse_parameters.yml

.PHONY = ci trigger
.DEFAULT_GOAL = ci

all: ci trigger

generate-ci:
	jinja2 --strict --format=yaml ${CONCOURSE_CONFIG_TEMPLATE} ${CONCOURSE_CONFIG_DATA} > ${CONCOURSE_CONFIG}

ci: generate-ci
	fly -t ${CONCOURSE_TARGET} \
		set-pipeline \
		-c ${CONCOURSE_CONFIG} \
		-p ${CONCOURSE_PIPELINE} \
		-l ${CONCOURSE_PARAMETERS} \
		-v http_proxy="${http_proxy}"
	fly -t ${CONCOURSE_TARGET} unpause-pipeline -p ${CONCOURSE_PIPELINE}

trigger:
	fly -t ${CONCOURSE_TARGET} trigger-job -w -j ${CONCOURSE_PIPELINE}/${CONCOURSE_JOB}
