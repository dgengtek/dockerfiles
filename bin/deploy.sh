#!/bin/bash
# cfg datei in das richtige Verzeichnis installieren
# docker image erstellen mit Hilfe der cfg
usage () {
  cat << EOF
Usage: ${0##*/} imagename


EOF
  exit 1
}
main() {
  local_config_files=(
  "manage_service_container.sh"
  )
  validity_check 

}
instal_service() {
  local -r service_template="dcon@.service"
  local -r tmp_dir=$(mktemp -d)
  pushd $tmp_dir
  git clone http://git/docker/bin.git


  cp -f "${service_template}.service" "/etc/systemd/system/"
  systemctl enable "${service_template}${s}.service"
  popd
  rm -rf $tmp_dir
}
instal_config() {
:
}
validity_check() {
  for i in $@; do
    if ! [ -f "$i" ]; then
      usage
    fi
  done
}

set -e
main "$@"
