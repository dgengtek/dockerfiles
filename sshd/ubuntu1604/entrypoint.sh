#!/bin/env bash
main() {
  ssh-keygen -A
  local -r auth_keys_dir="/root/.ssh/authorized_keys"
  if ! [[ -f "$auth_keys_dir" ]]; then
    echo "Authorized keys for ssh missing." >&2
    exit 1
  fi
  chown root:root "$auth_keys_dir"
  echo "Starting sshd."
  /usr/sbin/sshd -D || echo "Failed sshd startup."
}
main "$@"
