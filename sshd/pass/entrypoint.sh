#!/bin/env bash
main() {
    ssh-keygen -A
    local -r PASS=$(openssl rand -base64 6)
    echo "$PASS"
    echo "root:$PASS" | chpasswd


    /usr/sbin/sshd -D
}

main "$@"
